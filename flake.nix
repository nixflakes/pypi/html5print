{
  description = "atsr python libraries";

  inputs = {
    settings = {
      url = "path:./nix/settings.nix";
      flake = false;
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    ply = {
      url = "gitlab:nixflakes%2Fpypi/ply/3.4";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.settings.follows = "settings";
    };
    slimit = {
      url = "gitlab:nixflakes%2Fpypi/slimit/0.8.1";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.settings.follows = "settings";
    };
  };

  outputs = { self, flake-utils, ... }@inputs:

    flake-utils.lib.eachDefaultSystem (system: let

      settings = import inputs.settings;
      nixpkgs = inputs.nixpkgs.legacyPackages.${system};
      pythonPackages = nixpkgs.${settings.python+"Packages"};

      ply = inputs.ply.defaultPackage.${system};
      slimit = inputs.slimit.defaultPackage.${system};

      thispkg = pythonPackages.buildPythonPackage rec {
        pname = "html5print";
        version = "0.1.2";
        src = pythonPackages.fetchPypi {
          inherit pname version;
          sha256 = "1ff645698a7192a61f8a5963f23dc894d16fa3346804a2d1ac462683f6e44d4b";
        };
        propagatedBuildInputs = with pythonPackages; [ 
          beautifulsoup4
          chardet
          html5lib
          requests
          slimit
          tinycss2
          ply
        ];
        # prePatch = ''
        #   sed -i 's/ply==/ply>=/g' requirements.txt
        # '';
        doCheck = false;
      };

      pythonPackaged = nixpkgs.${settings.python}.withPackages (p: with p; [ thispkg ]);

    in {

      defaultPackage = thispkg;
        
      devShell = nixpkgs.mkShell {
        buildInputs = [ pythonPackaged ];
        shellHook = ''
          export PYTHONPATH=${pythonPackaged}/${pythonPackaged.sitePackages}
        '';
      };

    });
}
